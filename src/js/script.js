"use strict";

window.addEventListener("DOMContentLoaded", () => {
  // Tabs
  const tabs = document.querySelectorAll(".tabheader__item");
  const tabsContent = document.querySelectorAll(".tabcontent");
  const tabsParent = document.querySelector(".tabheader__items");

  function hideTabContent() {
    tabsContent.forEach(item => {
      item.classList.add("hide");
      item.classList.remove("show", "fade");
    });
    tabs.forEach(tab => {
      tab.classList.remove("tabheader__item_active");
    })
  }

  function showTabContent(i = 0) {
    tabsContent[i].classList.add("show", "fade");
    tabsContent[i].classList.remove("hide");
    tabs[i].classList.add("tabheader__item_active");
  }

  hideTabContent();
  showTabContent();

  tabsParent.addEventListener("click", (event) => {
    const target = event.target;
    if (target && target.classList.contains("tabheader__item")) {
      tabs.forEach((item, i) => {
        if (target === item) {
          hideTabContent();
          showTabContent(i);
        }
      });
    }
  });

  // Timer
  function getRemainingTime(endTime) {
    // Date.parse не читает "2022-12-7T22:11", а вот это ему подходит "2022-12-7 22:11"
    const diffMillis = Date.parse(endTime) - new Date().getTime();
    return {
      total: diffMillis,
      days: formatTimeUnit(Math.floor(diffMillis / (1000 * 60 * 60 * 24))),
      hours: formatTimeUnit(Math.floor((diffMillis / (1000 * 60 * 60)) % 24)),
      minutes: formatTimeUnit(Math.floor((diffMillis / (1000 * 60)) % 60)),
      seconds: formatTimeUnit(Math.floor((diffMillis / 1000) % 60))
    };
  }

  function formatTimeUnit(num) {
    return num >= 0 && num < 10 ? `0${num}` : num;
  }

  function setClock(selector, endTime) {
    const timer = document.querySelector(selector);
    const days = timer.querySelector("#days");
    const hours = timer.querySelector("#hours");
    const minutes = timer.querySelector("#minutes");
    const seconds = timer.querySelector("#seconds");
    const timeIntervalId = setInterval(updateClock, 1000);
    updateClock();

    function updateClock() {
      const remaining = getRemainingTime(endTime);
      days.textContent = remaining.days;
      hours.textContent = remaining.hours;
      minutes.textContent = remaining.minutes;
      seconds.textContent = remaining.seconds;

      if (remaining.total <= 1000) {
       days.textContent = "00";
       hours.textContent = "00";
       minutes.textContent = "00";
       seconds.textContent = "00";
       clearInterval(timeIntervalId);
      }
    }
  }

  const deadline = "2022-12-15 22:50:40";
  setClock(".timer", deadline);

  // Modal
  const modalTrigger = document.querySelectorAll("[data-modal]");
  const modal = document.querySelector(".modal");
  const modalCloseButton = document.querySelector(".modal__close");
  const modalTimerId = setTimeout(openModal, 3000);

  function openModal() {
    modal.classList.add("show");
    modal.classList.remove("hide");
    document.body.style.overflow = "hidden";
    clearTimeout(modalTimerId);  // Не покажется, если пользователь сам открыл окно до таймаута
  }

  function closeModal() {
    modal.classList.add("hide");
    modal.classList.remove("show");
    document.body.style.overflow = "";
  }

  modalTrigger.forEach(trigger => trigger.addEventListener("click", openModal));

  modalCloseButton.addEventListener("click", closeModal);

  modal.addEventListener("click", (event) => {
    if (event.target === modal) {
      closeModal();
    }
  });

  document.addEventListener("keydown", (event) => {
    if (event.code === "Escape" && modal.classList.contains("show")) {
      console.log(event.code);
      closeModal();
    }
  });

  function showModalByScroll() {
    const docElement = document.documentElement;
    if (window.scrollY + docElement.clientHeight >= docElement.scrollHeight) {
      openModal();
      window.removeEventListener("scroll", showModalByScroll);
    }
  } 

  window.addEventListener("scroll", showModalByScroll);

  // Cards
  class MenuCard {
    constructor(src, alt, title, description, price, parentSelector, ...classes) {
      this.src = src;
      this.alt = alt;
      this.title = title;
      this.description = description;
      this.price = price;
      this.classes = classes;

      this.parent = document.querySelector(parentSelector);
      this.exchangeRate = 27;
      this.changeToUAH();
    }

    changeToUAH() {
      this.price *= this.exchangeRate;
    }

    render() {
      const card = document.createElement("div");
      if (this.classes.length === 0) {
        card.classList.add("menu__item");
      } else {
        this.classes.forEach(className => card.classList.add(className));
      }

      card.innerHTML = `
        <img src="${this.src}" alt="${this.alt}">
        <h3 class="menu__item-subtitle">${this.title}</h3>
        <div class="menu__item-descr">${this.description}</div>
        <div class="menu__item-divider"></div>
        <div class="menu__item-price">
            <div class="menu__item-cost">Цена:</div>
            <div class="menu__item-total"><span>${this.price}</span> грн/день</div>
        </div>
      `;
      this.parent.append(card);
    }
  }

  new MenuCard(
      "img/tabs/vegy.jpg",
      "vegy",
      `Меню “Фитнес“`,
      "Меню ”Фитнес” - это новый подход к приготовлению блюд: больше свежих овощей и фруктов. Продукт активных и здоровых людей. Это абсолютно новый продукт с оптимальной ценой и высоким качеством!",
      9,
      ".menu .container",
      "menu__item",
      "big"
  ).render();

  new MenuCard(
      "img/tabs/elite.jpg",
      "elite",
      `Меню “Премиум”`,
      "В меню ”Премиум” мы используем не только красивый дизайн упаковки, но и качественное исполнение блюд. Красная рыба, морепродукты, фрукты - ресторанное меню без похода в ресторан!",
      14,
      ".menu .container",
  ).render();

  new MenuCard(
      "img/tabs/post.jpg",
      "post",
      `Меню “Постное”`,
      "Меню “Постное” - это тщательный подбор ингредиентов: полное отсутствие продуктов животного происхождения, молоко из миндаля, овса, кокоса или гречки, правильное количество белков за счет тофу и импортных вегетарианских стейков.",
      21,
      ".menu .container",
  ).render();
});
